import pymssql # for sql server
# import mssql_api


from flask import Flask, render_template, redirect, url_for, request, session, jsonify, abort, app, g, flash, send_from_directory
from flask_bootstrap import Bootstrap
from flask_login import LoginManager, UserMixin, \
						login_required, login_user, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, DateTimeField, IntegerField, BooleanField, PasswordField, SubmitField, TextAreaField
from wtforms.validators import InputRequired, Email, Length, AnyOf, DataRequired
from wtforms.widgets import TextArea


from flask_wtf.csrf import CSRFProtect

from wtforms_components import DateTimeField
from wtforms.fields.html5 import DateField, DateTimeField, EmailField
from werkzeug.datastructures import MultiDict

import os, socket, logging

import queries

host = os.getenv('DB_HOST', 'localhost')
username = 'py_app'
password = 'py_app321!'
database = 'ITDB_dev'
port = '1433'

con = pymssql.connect(host = host, user = username, password = password, database = database, port = port, autocommit=True)

app = Flask(__name__)
app.secret_key = 'secret'
Bootstrap(app)
login_manager = LoginManager()
login_manager.init_app(app)

# user_instances = set()
class User(UserMixin):
	def __init__(self, user_id):
		cur_u = con.cursor(as_dict=True)
		cur_u.execute(queries.load_user_data, {'userid' : user_id})
		user_data = cur_u.fetchone()
		cur_u.close()
		# app.logger.debug(user_data)
		self.id = user_data['UserId']
		self.username = user_data['UserName']
		self.userclassid = user_data['UserClassId']
		self.teamid = user_data['TeamId']
		self.f_name = user_data['F_Name']
		self.l_name = user_data['L_Name']
		self.email = user_data['Email']
		self.ismanager = user_data['IsManager']
		self.lastlogin = user_data['LastLogin']
		self.isops = user_data['IsOps']
		self.teamname = user_data['TeamName']
		self.teamdesc = user_data['TeamDesc']
		self.teamclassname = user_data['TeamClassName']
		self.teamclassid = user_data['TeamClassId']
		# user_instances.add(self.id)

class LoginForm(FlaskForm):
	username = StringField('שם משתמש', validators=[InputRequired()])
	password = PasswordField('סיסמה', \
				validators=[InputRequired(), Length(min=2, max=10)])

class SelectFieldNoValidation(SelectField):
	def pre_validate(self, form):
		pass

class OpenTicketForm(FlaskForm):
	severitylist = SelectField('דחיפות', default=2, coerce=int)
	category1list = SelectFieldNoValidation('תחום', choices = (), coerce=int)
	category2list = SelectFieldNoValidation('תת תחום', choices = ())
	category3list = SelectFieldNoValidation('פניה', choices = ())
	note = StringField(u'תיאור פניה', widget=TextArea())

class AddTicketNoteForm(FlaskForm):
	note_text = TextAreaField('Note', render_kw={"class_": "form-control", "rows": 5, "cols": 200, "style": "resize: none;"})
	close_reason = SelectFieldNoValidation('close_reason', choices = ())
	close_ticket = BooleanField('close')
	# is_fix = BooleanField('fix', default = True)

class UpdateP_TicketStatusForm(FlaskForm):
	ticket_status = SelectField('סטאטוס', choices = (), render_kw={'disabled':''})

@app.route("/login", methods=["GET", "POST"])	
def login():
	cur = con.cursor(as_dict=True)
	loginform = LoginForm()
	if request.method == 'POST':
		# app.logger.debug(loginform.errors)
		# app.logger.debug(loginform.validate_on_submit())
		if loginform.validate_on_submit():
			username = request.form['username']
			password = request.form['password']
			cur.execute(queries.login_user, {'username' : username, 'passkey' : password})
			d_userid = cur.fetchone()
			# cur.close()
			if d_userid is not None:
				user = User(d_userid['userid'])
				login_user(user)
				flash('{}, התחברת בהצלחה'.format(current_user.f_name), 'success')
				# 	cur.execute(queries.update_emp_last_login, ({'userid' : current_user.id}))
				return redirect(url_for('index'))
			else:
				flash('Login faild.', 'error')
	cur.execute('SELECT u.UserName, u.PassKey FROM dbo.Users u')
	userpasslist = d_userid = cur.fetchall()
	cur.close()
	return render_template('login.html', loginform=loginform, userpasslist=userpasslist)

@app.route("/logout")
@login_required
def logout():
	# user_instances.remove(current_user.id)
	logout_user()
	return redirect(url_for('index'))

@login_manager.user_loader
def load_user(user_id):
	return User(user_id)

@app.route("/")
def index():
	return redirect(url_for('home'))

@app.route("/home")
def home():
	return render_template('index.html')

# @app.route("/test1")
# def test1():
# 	return render_template('test1.html')

@app.route("/rts_main")
@login_required
def rts_main():
	cur = con.cursor(as_dict=True)
	cur.execute(queries.get_opentickets_cust, {'userid' : current_user.id})
	opentickets_cust = cur.fetchall()
	cur.close()
	return render_template('rts_main.html', opentickets_cust = opentickets_cust)

@app.route("/open-ticket", methods=["GET", "POST"])
@login_required
def open_ticket():
	cur = con.cursor()
	cur.execute(queries.get_severity_list)
	cur_severitylist = cur.fetchall()
	openticketform = OpenTicketForm()
	openticketform.severitylist.choices = cur_severitylist
	if request.method == 'POST':
		# app.logger.debug(openticketform.errors)
		if openticketform.validate_on_submit():
			cur.callproc('rts.sp_OpenTicket', (request.form['severitylist'],
												request.form['category1list'],
												request.form['category2list'],
												request.form['category3list'],
												request.form['note'],
												current_user.id
													))
			cur.close()
			flash('הפניה נפתחה בהצלחה', 'success')
			return redirect(url_for('rts_main'))
	cur.close()
	return render_template('open_ticket.html', openticketform = openticketform)

@app.route("/my-team-open-ticket", methods=["GET", "POST"])
@login_required
def my_team_open_ticket():
	cur = con.cursor(as_dict=True)
	cur.execute(queries.get_opentickets_ops, {'teamid' : current_user.teamid})
	opentickets_ops = cur.fetchall()
	cur.close()
	return render_template('myteam_open_ticket.html', opentickets_ops = opentickets_ops)

@app.route("/my-team_closed_ticket", methods=["GET", "POST"])
@login_required
def my_team_closed_ticket():
	cur = con.cursor(as_dict=True)
	cur.execute(queries.get_closedtickets_ops, {'teamid' : current_user.teamid})
	opentickets_ops = cur.fetchall()
	cur.close()
	return render_template('myteam_closed_ticket.html', opentickets_ops = opentickets_ops)

@app.route("/ticket-p/<int:ticket_id>", methods=["GET", "POST"])
@login_required
def ticket_p(ticket_id):
	addticketnoteform = AddTicketNoteForm()
	updatep_ticketstatusform = UpdateP_TicketStatusForm()
	cur = con.cursor(as_dict=True)
	cur.execute(queries.get_ticket_header, {'ticketid' : ticket_id})
	ticket_header = cur.fetchone()
	cur = con.cursor()
	app.logger.debug(ticket_header['TicketStatusId'])
	cur.execute(queries.get_ticketstatus, {'ticketstatusid' : ticket_header['TicketStatusId']})
	ticket_status = cur.fetchall()
	cur.close()

	cur = con.cursor()
	cur.execute(queries.get_close_reasons)
	close_reasons = cur.fetchall()
	cur.close()
	addticketnoteform.close_reason.choices = close_reasons

	ticket_status.insert(0, [ticket_header['TicketStatusId'], ticket_header['TicketStatusText']])
	updatep_ticketstatusform.ticket_status.choices = ticket_status
	return render_template('ticket_p.html',
				ticket_header = ticket_header,
				addticketnoteform = addticketnoteform,
				updatep_ticketstatusform = updatep_ticketstatusform)

@app.route('/_update_ticket_status', methods=["POST"])
@login_required
def update_ticket_status():
	ticket_status = request.form['ticket_status']
	ticketid = request.form['ticketid']
	cur2 = con.cursor()
	cur2.callproc('rts.sp_UpdateTicketStatus',(ticketid, ticket_status, current_user.id))
	cur2.close()
	return jsonify(None)


@app.route('/_get_notes_hist')
@login_required
def get_notes_hist():
	cur1 = con.cursor(as_dict=True)
	ticketid = request.args.get('ticketid', 0, type=int)
	cur1.execute(queries.get_notes_hist, {'ticketid' : ticketid})
	notes = cur1.fetchall()
	cur1.close()
	return jsonify(notes)

@app.route('/_add_note', methods=["POST"])
@login_required
def add_note():
	note_text = request.form['note_text']
	ticketid = request.form['ticketid']
	app.logger.debug(request.form['close_ticket'])
	close_reason = request.form['close_reason']
	note_status_type = 2
	if request.form['close_ticket'] == 'true':
		note_status_type = 3
	app.logger.debug(note_status_type)
	cur = con.cursor()
	cur.callproc('rts.sp_AddNote',(ticketid, note_status_type, note_text, current_user.id, close_reason))
	cur.close()
	return jsonify(note_status_type)

@app.route('/_update_category1')
@login_required
def update_category1():
	cur = con.cursor(as_dict=True)
	cur.execute(queries.get_category1)
	category1list = cur.fetchall()
	cur.close()
	return jsonify(category1list)


@app.route('/_update_category2')
@login_required
def update_category2():
	category1id = request.args.get('category1id', 0, type=int)
	cur = con.cursor(as_dict=True)
	cur.execute(queries.get_category2, {'parentcategory1id' : category1id})
	category2list = cur.fetchall()
	cur.close()
	return jsonify(category2list)

@app.route('/_update_category3')
@login_required
def update_category3():
	category2id = request.args.get('category2id', 0, type=int)
	cur = con.cursor(as_dict=True)
	cur.execute(queries.get_category3, {'parentcategory2id' : category2id})
	category3list = cur.fetchall()
	app.logger.debug(category3list)
	cur.close()
	return jsonify(category3list) 

from os import path
extra_dirs = ['templates','static']
extra_files = extra_dirs[:]
for extra_dir in extra_dirs:
	for dirname, dirs, files in os.walk(extra_dir):
		for filename in files:
			filename = path.join(dirname, filename)
			if path.isfile(filename):
				extra_files.append(filename)

if __name__ == '__main__':
	# debug=config[args.env]['flask_debug']
	import errors
	app.config['DEBUG'] = True
	# debug=True
	# threaded=True
	app.run(host='0.0.0.0',debug=True,threaded=True, port=5001, extra_files=extra_files)