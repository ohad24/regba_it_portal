login_user = """
	SELECT UserId AS userid FROM dbo.Users u
	WHERE u.UserName = %(username)s AND u.PassKey = %(passkey)s"""

load_user_data = """
	SELECT u.UserId, u.UserName, u.UserClassId, u.TeamId, u.F_Name,
			u.L_Name, u.Email, u.IsManager, u.LastLogin,
			tc.IsOps, t.TeamName, t.TeamDesc, tc.TeamClassName, tc.TeamClassId
	FROM dbo.Users u, dbo.Teams t, dbo.TeamClass tc
	WHERE u.TeamId = t.TeamId
	AND t.TeamClassId = tc.TeamClassId
	AND u.UserId = %(userid)s"""

get_severity_list = """
	SELECT SeverityId, SeverityText FROM rts.RefSeverity ORDER BY 1"""

get_category1 = """
	SELECT Category1Id, Category1Text FROM rts.RefCategory1 order by 1"""

get_category2 = """
	SELECT Category2Id, Category2Text FROM rts.RefCategory2 
	WHERE ParentCategory1Id = %(parentcategory1id)s
	ORDER BY 1"""

get_category3 = """
	SELECT Category3Id, Category3Text FROM rts.RefCategory3
	WHERE ParentCategory2Id = %(parentcategory2id)s
	ORDER BY 1 """

get_opentickets_cust = """
	SELECT * FROM rts.v_TicketsHeader th
	WHERE th.CreateBy = %(userid)s
	AND th.CloseDate IS NULL
	ORDER BY th.SLADate"""

get_opentickets_ops = """
	SELECT * FROM rts.v_TicketsHeader th
	WHERE EXISTS (
		SELECT 1 FROM rts.TeamsCat1Assignment tc1
		WHERE th.Category1Id = tc1.Category1Id
		AND tc1.TeamId = %(teamid)s)
	AND th.TicketStatusId != 4
	ORDER BY th.SLADate"""

get_closedtickets_ops = """
	SELECT * FROM rts.v_TicketsHeader th
	WHERE EXISTS (
		SELECT 1 FROM rts.TeamsCat1Assignment tc1
		WHERE th.Category1Id = tc1.Category1Id
		AND tc1.TeamId = %(teamid)s)
	AND th.TicketStatusId = 4
	ORDER BY th.CloseDate"""

get_ticket_header = """
	SELECT * FROM rts.v_TicketsHeader th
	WHERE th.TicketId = %(ticketid)s"""

get_notes_hist = """
	SELECT n.NoteId, n.TicketId, n.NoteTypeId, nt.NoteTypeText,
	n.NoteText, u.F_Name + ' ' + u.L_Name AS E_Name, rts.f_ConvertHebDate(n.CreateDate) CreateDate
	FROM rts.Notes n, rts.RefNoteType nt, dbo.Users u
	WHERE 1=1
	AND n.NoteTypeId = nt.NoteTypeId
	AND n.CreateBy = u.UserId
	AND n.NoteTypeId in (2,3,4)
	AND n.TicketId = %(ticketid)s
	ORDER BY n.CreateDate DESC"""

get_ticketstatus = """
	SELECT * FROM rts.RefTicketStatus ts
	WHERE ts.TicketStatusId NOT IN (4)
	AND ts.TicketStatusId != %(ticketstatusid)s -- done"""

get_close_reasons = """SELECT * FROM rts.RefCloseReason ORDER BY 1"""