USE master
GO

DECLARE @sql varchar(255);
DECLARE @DBN VARCHAR(MAX);
SET @DBN = N'ITDB_dev';
IF EXISTS (SELECT name FROM sys.databases WHERE name = @DBN)
	SET @sql = 'ALTER DATABASE ' + @DBN + ' SET SINGLE_USER WITH ROLLBACK IMMEDIATE'
	EXEC (@sql)
	SET @sql = 'DROP DATABASE ' + @DBN
	EXEC (@sql)

CREATE DATABASE ITDB_dev COLLATE Hebrew_CS_AS;

USE ITDB_dev
GO

IF EXISTS (SELECT name FROM sys.schemas WHERE name = N'rts')
	DROP SCHEMA rts;

EXEC ('CREATE SCHEMA rts;');

--USE master
--GO

--DROP LOGIN py_app;
IF EXISTS (SELECT loginname FROM dbo.syslogins 
    WHERE name = 'py_app' AND dbname = 'ITDB_dev')
	DROP LOGIN py_app

CREATE LOGIN py_app WITH PASSWORD=N'py_app321!', DEFAULT_DATABASE = ITDB_dev, DEFAULT_LANGUAGE = US_ENGLISH
GO

ALTER LOGIN py_app ENABLE
GO

-- USE ITDB_dev
-- GO

--DROP USER py_app_user
CREATE USER py_app_user FOR LOGIN py_app WITH DEFAULT_SCHEMA = [DBO]
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON SCHEMA :: dbo TO py_app_user;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE ON SCHEMA :: rts TO py_app_user;