USE ITDB_dev
GO

IF OBJECT_ID('rts.TeamsCat1Assignment', 'U') IS NOT NULL
	DROP TABLE rts.TeamsCat1Assignment;
IF OBJECT_ID('rts.NoteFiles', 'U') IS NOT NULL
	DROP TABLE rts.NoteFiles;
IF OBJECT_ID('rts.Notes', 'U') IS NOT NULL
	DROP TABLE rts.Notes;
IF OBJECT_ID('rts.Tickets', 'U') IS NOT NULL
	DROP TABLE rts.Tickets;
IF OBJECT_ID('rts.RefCloseReason', 'U') IS NOT NULL
	DROP TABLE rts.RefCloseReason;
IF OBJECT_ID('rts.RefSeverity', 'U') IS NOT NULL
	DROP TABLE rts.RefSeverity;
IF OBJECT_ID('rts.RefCategory3', 'U') IS NOT NULL
	DROP TABLE rts.RefCategory3;
IF OBJECT_ID('rts.RefCategory2', 'U') IS NOT NULL
	DROP TABLE rts.RefCategory2;
IF OBJECT_ID('rts.RefCategory1', 'U') IS NOT NULL
	DROP TABLE rts.RefCategory1;
IF OBJECT_ID('rts.RefNoteType', 'U') IS NOT NULL
	DROP TABLE rts.RefNoteType;
IF OBJECT_ID('rts.RefTicketStatus', 'U') IS NOT NULL
	DROP TABLE rts.RefTicketStatus;
	
/*
	DROP SCHEMA rts
	CREATE SCHEMA rts
*/
-----REF TABLES-----

CREATE TABLE rts.RefNoteType (
	NoteTypeId INT PRIMARY KEY IDENTITY(1,1),
	NoteTypeText NVARCHAR(30) UNIQUE NOT NULL
);

CREATE TABLE rts.RefCategory1 (
	Category1Id INT PRIMARY KEY IDENTITY(1,1),
	Category1Text NVARCHAR(30) UNIQUE NOT NULL
);

CREATE TABLE rts.RefCategory2 (
	Category2Id INT PRIMARY KEY IDENTITY(1,1),
	Category2Text NVARCHAR(30) NOT NULL,
	ParentCategory1Id INT FOREIGN KEY REFERENCES rts.RefCategory1(Category1Id),
	CONSTRAINT UNQ_RefCategory2 UNIQUE (Category2Text, ParentCategory1Id)
);

CREATE TABLE rts.RefCategory3 (
	Category3Id INT PRIMARY KEY IDENTITY(1,1),
	Category3Text NVARCHAR(30) NOT NULL,
	ParentCategory2Id INT FOREIGN KEY REFERENCES rts.RefCategory2(Category2Id),
	SLA_time DECIMAL(10,2) NOT NULL
	CONSTRAINT UNQ_RefCategory3 UNIQUE (Category3Text, ParentCategory2Id)
);

CREATE TABLE rts.RefSeverity (
	SeverityId INT PRIMARY KEY IDENTITY(1,1),
	SeverityText NVARCHAR(30) UNIQUE NOT NULL
);

CREATE TABLE rts.RefCloseReason (
	CloseReasonId INT PRIMARY KEY IDENTITY(1,1),
	CloseReasonText NVARCHAR(30) UNIQUE NOT NULL
);

CREATE TABLE rts.RefTicketStatus (
	TicketStatusId INT PRIMARY KEY IDENTITY(1,1),
	TicketStatusText NVARCHAR(30) UNIQUE NOT NULL
);



-----DATA TABLES-----

CREATE TABLE rts.Tickets (
	TicketId INT PRIMARY KEY IDENTITY(1,1),
	TicketStatusId INT NOT NULL FOREIGN KEY REFERENCES rts.RefTicketStatus(TicketStatusId),
	CustSeverityId INT NOT NULL FOREIGN KEY REFERENCES rts.RefSeverity(SeverityId),
	ManagerSeverityId INT FOREIGN KEY REFERENCES rts.RefSeverity(SeverityId),
	Category1Id INT NOT NULL FOREIGN KEY REFERENCES rts.RefCategory1(Category1Id),
	Category2Id INT NOT NULL FOREIGN KEY REFERENCES rts.RefCategory2(Category2Id),
	Category3Id INT NOT NULL FOREIGN KEY REFERENCES rts.RefCategory3(Category3Id),
	OpenDate DATETIME NOT NULL,
	
	SLAOrigDate DATETIME NOT NULL,
	SLAManagerDate DATETIME,
	SLAManagerBy INT,
	AssignTo INT,
	AssignDate DATETIME,
	AssignManagerNote NVARCHAR(MAX),
	AssignBy INT,
	
	CloseDate DATETIME,
	CloseBy INT,
	CloseReasonId INT FOREIGN KEY REFERENCES rts.RefCloseReason(CloseReasonId),
	CreateBy INT,
	CreateDate DATETIME,
	LastUpdateBy INT,
	LastUpdateDate DATETIME
);

CREATE TABLE rts.Notes (
	NoteId INT PRIMARY KEY IDENTITY(1,1),
	TicketId INT NOT NULL FOREIGN KEY REFERENCES rts.Tickets(TicketId),
	NoteTypeId INT NOT NULL FOREIGN KEY REFERENCES rts.RefNoteType(NoteTypeId),
	NoteText NVARCHAR(MAX) NOT NULL,
	CreateBy INT,
	CreateDate DATETIME,
	LastUpdateBy INT,
	LastUpdateDate DATETIME
);

CREATE TABLE rts.NoteFiles (
	NoteFileId INT PRIMARY KEY IDENTITY(1,1),
	NoteId INT NOT NULL FOREIGN KEY REFERENCES rts.Notes(NoteId),
	FileNum INT NOT NULL,
	FileOriginName NVARCHAR(MAX),
	FileAlterName NVARCHAR(MAX)
)
-----ADD REF FORIGN KEYS-----


-----User-----

IF OBJECT_ID('dbo.Users', 'U') IS NOT NULL
	DROP TABLE dbo.Users;
IF OBJECT_ID('dbo.UserClass', 'U') IS NOT NULL
	DROP TABLE dbo.UserClass;
IF OBJECT_ID('dbo.Teams', 'U') IS NOT NULL
	DROP TABLE dbo.Teams;
IF OBJECT_ID('dbo.TeamClass', 'U') IS NOT NULL
	DROP TABLE dbo.TeamClass;

CREATE TABLE dbo.TeamClass (
	TeamClassId INT PRIMARY KEY IDENTITY(1,1),
	TeamClassName NVARCHAR(30) UNIQUE NOT NULL,
	IsOps BIT NOT NULL
)

CREATE TABLE dbo.Teams (
	TeamId INT PRIMARY KEY IDENTITY(1,1),
	TeamName NVARCHAR(20) UNIQUE NOT NULL,
	TeamDesc NVARCHAR(MAX),
	TeamClassId INT NOT NULL FOREIGN KEY REFERENCES dbo.TeamClass(TeamClassId)
)

CREATE TABLE dbo.UserClass (
	UserClassId INT PRIMARY KEY IDENTITY(1,1),
	UserClassName NVARCHAR(30) UNIQUE NOT NULL
)

CREATE TABLE dbo.Users (
	UserId INT PRIMARY KEY IDENTITY(1,1),
	UserName NVARCHAR(20) UNIQUE NOT NULL,
	UserClassId INT NOT NULL FOREIGN KEY REFERENCES dbo.UserClass(UserClassId),
	TeamId INT NOT NULL FOREIGN KEY REFERENCES dbo.Teams(TeamId),
	F_Name NVARCHAR(30) NOT NULL,
	L_Name NVARCHAR(30) NOT NULL,
	Email NVARCHAR(50) UNIQUE NOT NULL,
	IsManager BIT NOT NULL,
	PassKey NVARCHAR(30) NOT NULL,
	IsBlock BIT NOT NULL,
	IsAuth BIT NOT NULL,
	LastLogin DATETIME,
	CreateBy INT,
	CreateDate DATETIME,
	LastUpdateBy INT,
	LastUpdateDate DATETIME
)

-- DBCC CHECKIDENT ('dbo.Users', RESEED, 0)


----- COLLECT TABLES ------
CREATE TABLE rts.TeamsCat1Assignment (
Tc1Id INT PRIMARY KEY IDENTITY(1,1),
	TeamId INT NOT NULL FOREIGN KEY REFERENCES dbo.Teams(TeamId),
	Category1Id INT NOT NULL FOREIGN KEY REFERENCES rts.RefCategory1(Category1Id),
	CONSTRAINT UNQ_TeamsCat1Assignment UNIQUE (TeamId, Category1Id)
)