USE [ITDB_dev]
GO

CREATE   VIEW [rts].[v_TicketsHeader] AS
SELECT 
t.*,
rs.SeverityText AS CustSeverity,
rs_m.SeverityText AS ManagerSeverity,
ISNULL(rs_m.SeverityText, rs.SeverityText) AS SeverityText,
ISNULL(t.SLAManagerDate, t.SLAOrigDate) SLADate,
rc1.Category1Text,
rc2.Category2Text,
rc3.Category3Text,
rc3.SLA_time,
u1.F_Name + ' ' + u1.L_Name AS CreateByName,
u2.F_Name + ' ' + u2.L_Name AS CloseByName,
u3.F_Name + ' ' + u3.L_Name AS AssignToName,
u4.F_Name + ' ' + u4.L_Name AS AssignByName,
u5.F_Name + ' ' + u4.L_Name AS UpdateByName,
n.NoteText OpenNoteText,
ts.TicketStatusText,
cr.CloseReasonText,
rts.f_ConvertHebDate(t.OpenDate) OpenDateHeb,
rts.f_ConvertHebDate(t.SLAOrigDate) SLAOrigDateHeb,
rts.f_ConvertHebDate(t.SLAManagerDate) SLAManagerDateHeb,
rts.f_ConvertHebDate(ISNULL(t.SLAManagerDate, t.SLAOrigDate)) SLADateHeb,
rts.f_ConvertHebDate(t.CloseDate) CloseDateHeb,
rts.f_ConvertHebDate(t.AssignDate) AssignDateHeb,
rts.f_ConvertHebDate(t.LastUpdateDate) LastUpdateDateHeb
FROM rts.Tickets t
INNER JOIN rts.RefSeverity rs ON t.CustSeverityId = rs.SeverityId
LEFT JOIN rts.RefSeverity rs_m ON t.ManagerSeverityId = rs_m.SeverityId
INNER JOIN rts.RefCategory1 rc1 ON t.Category1Id = rc1.Category1Id
INNER JOIN rts.RefCategory2 rc2 ON t.Category2Id = rc2.Category2Id
INNER JOIN rts.RefCategory3 rc3 ON t.Category3Id = rc3.Category3Id
INNER JOIN dbo.Users u1 ON t.CreateBy = u1.UserId --or t.CloseBy = u.UserId
LEFT JOIN dbo.Users u2 ON t.CloseBy = u2.UserId
LEFT JOIN dbo.Users u3 ON t.AssignTo = u3.UserId
LEFT JOIN dbo.Users u4 ON t.AssignBy = u4.UserId
LEFT JOIN dbo.Users u5 ON t.LastUpdateBy = u5.UserId
INNER JOIN rts.Notes n ON t.TicketId = n.TicketId and n.NoteTypeId = 1
INNER JOIN rts.RefNoteType nt ON n.NoteTypeId = nt.NoteTypeId
INNER JOIN rts.RefTicketStatus ts ON t.TicketStatusId = ts.TicketStatusId
LEFT JOIN rts.RefCloseReason cr ON t.CloseReasonId = cr.CloseReasonId
WHERE 1=1
GO

CREATE VIEW [rts].[v_TeamsAndClasses] AS
	SELECT t.TeamId, t.TeamName, t.TeamDesc, tc.TeamClassName FROM dbo.Teams t, dbo.TeamClass tc
	WHERE t.TeamClassId = tc.TeamClassId
GO