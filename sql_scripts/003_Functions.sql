USE [ITDB_dev]
GO

CREATE   FUNCTION [rts].[CalcWorkHours](@s_datetime DATETIME, @totalhours decimal(6,2))
RETURNS DATETIME
AS
BEGIN
	declare @workhours INT, @s_time time = '08:00:00', @e_time time = '17:00:00',
		@e_datetime DATETIME, @hoursleft INT
	SET @hoursleft = @totalhours
	WHILE @hoursleft > 0
		BEGIN
			IF DATEPART(weekday , @s_datetime) BETWEEN 1 AND 5
				BEGIN
				IF CAST(@s_datetime AS TIME) BETWEEN @s_time AND @e_time
					BEGIN
					SET @e_datetime = DATEADD(day, CAST(@totalhours / 24 AS INT), @s_datetime);
					--PRINT @e_datetime
					SET @hoursleft = @hoursleft - CAST(@totalhours / 24 AS INT) * 24;
					SET @e_datetime = DATEADD(HOUR, @totalhours % 24, @e_datetime);
					SET @hoursleft = @hoursleft - @totalhours % 24
					END
				END
			ELSE
				BEGIN
					SET	@s_datetime = DATEADD(DAY, 1, @s_datetime);
				END


			IF CONVERT(TIME, @s_datetime) < @s_time
				BEGIN
				--PRINT @s_datetime
				SET @s_datetime = LEFT(CONVERT(nvarchar, @s_datetime, 120), 11) + N'08:00:00';
				END
			ELSE IF CONVERT(TIME, @s_datetime) > @e_time
				SET	@s_datetime = DATEADD(DAY, 1, @s_datetime);
				SET @s_datetime = LEFT(CONVERT(nvarchar, @s_datetime, 120), 11) + N'08:00:00';
				
				
		END
	
	WHILE DATEPART(weekday , @e_datetime) BETWEEN 6 AND 7 OR CONVERT(TIME, @e_datetime) NOT BETWEEN @s_time AND @e_time
		IF DATEPART(weekday , @e_datetime) BETWEEN 6 AND 7
			BEGIN
				SET	@e_datetime = DATEADD(DAY, 7 - DATEPART(weekday , @e_datetime) + 1, @e_datetime);
				SET @e_datetime = LEFT(CONVERT(nvarchar, @e_datetime, 120), 11) + N'08:00:01';
				--PRINT @e_datetime
			END
		ELSE 
			BEGIN
			IF CONVERT(TIME, @e_datetime) < @s_time
				BEGIN
					SET @e_datetime = LEFT(CONVERT(nvarchar, @e_datetime, 120), 11) + N'08:00:00';
				END
			ELSE IF CONVERT(TIME, @e_datetime) > @E_time
				BEGIN
					SET	@e_datetime = DATEADD(DAY, 1, @e_datetime);
					SET @e_datetime = LEFT(CONVERT(nvarchar, @e_datetime, 120), 11) + N'08:00:00';
				END
			END
	--PRINT @e_datetime
	RETURN @e_datetime
END
GO

CREATE   FUNCTION [rts].[f_ConvertHebDate] (@DATE DATETIME)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	RETURN FORMAT( @DATE, 'HH:mm ', 'he-il' ) + FORMAT(@DATE, 'D', 'he-il')
END
GO
