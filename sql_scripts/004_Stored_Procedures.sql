USE [ITDB_dev]
GO

CREATE   PROCEDURE [rts].[sp_OpenTicket] (@CustSeverityId INT, @Category1Id INT, @Category2Id INT, @Category3Id INT, @NoteText VARCHAR(MAX), @CreateBy INT)
AS
BEGIN

	DECLARE @TicketId_TBL TABLE( TicketId INT)
	DECLARE @TicketId INT
	DECLARE @SLA_START_DATETIME DATETIME

	--SELECT @CustSeverityId = 1, @Category1Id = 1, @Category2Id = 1 , @Category3Id = 1, @CreateBy = 1,  @NoteText = 'aaa'
	--SELECT @CreateBy = 1, @NoteText = 'aaa'
	
	SELECT @SLA_START_DATETIME = DATEADD(HOUR, SLA_time, GETDATE()) from rts.RefCategory3 WHERE Category3Id = @Category3Id
	SELECT @SLA_START_DATETIME =rts.CalcWorkHours(GETDATE(), SLA_time) from rts.RefCategory3 WHERE Category3Id = @Category3Id

	INSERT INTO rts.Tickets (TicketStatusId, CustSeverityId, Category1Id, Category2Id, Category3Id, OpenDate, SLAOrigDate, CreateBy, CreateDate)	
	OUTPUT Inserted.TicketId INTO @TicketId_TBL
		VALUES
			(1, @CustSeverityId, @Category1Id, @Category2Id, @Category3Id, GETDATE(), @SLA_START_DATETIME, @CreateBy, GETDATE())
	
	SELECT @TicketId = TicketId FROM @TicketId_TBL
		
	INSERT INTO rts.Notes (TicketId, NoteTypeId, NoteText, CreateBy, CreateDate)
		VALUES (@TicketId, 1, @NoteText, @CreateBy, GETDATE())

END;
GO

CREATE PROCEDURE [rts].[sp_AddNote] (@TicketId INT, @NoteTypeId INT, @NoteText NVARCHAR(MAX), @UID INT, @CloseReasonId INT = NULL)
AS
BEGIN
	INSERT INTO rts.Notes (TicketId, NoteTypeId, NoteText, CreateBy, CreateDate)
		VALUES (@TicketId, @NoteTypeId, @NoteText, @UID, GETDATE());

	IF @CloseReasonId IS NOT NULL
		UPDATE rts.Tickets SET TicketStatusId = 4,
							CloseDate = GETDATE(),
							CloseBy = @UID,
							CloseReasonId = @CloseReasonId
		WHERE TicketId = @TicketId;
END;
GO

CREATE   PROCEDURE [rts].[sp_UpdateTicketStatus] (@TicketId INT, @TicketStatusId INT, @LastUpdateBy INT)
AS
BEGIN
	DECLARE @NoteTextTBL TABLE (NoteIdBefore INT, NoteIdAfter INT);
	DECLARE @NoteTextBefore NVARCHAR(MAX), @NoteTextAfter NVARCHAR(MAX)

	UPDATE rts.Tickets SET TicketStatusId = @TicketStatusId, LastUpdateBy = @LastUpdateBy, LastUpdateDate = GETDATE()
	OUTPUT deleted.TicketStatusId , inserted.TicketStatusId INTO @NoteTextTBL
	FROM rts.Tickets t
	LEFT JOIN rts.RefNoteType nt ON t.TicketStatusId = nt.NoteTypeId
	WHERE TicketId = @TicketId;

	SELECT @NoteTextBefore = nt_b.TicketStatusText, @NoteTextAfter = nt_a.TicketStatusText FROM @NoteTextTBL t, rts.RefTicketStatus nt_b, rts.RefTicketStatus nt_a
	WHERE t.NoteIdBefore = nt_b.TicketStatusId
	and t.NoteIdAfter = nt_a.TicketStatusId

	
	DECLARE @NoteText NVARCHAR(MAX)
	SELECT @NoteText = 'שונה מ- ' + @NoteTextBefore + ' -ל ' + @NoteTextAfter FROM @NoteTextTBL
	EXEC rts.sp_AddNote @TicketId, 4, @NoteText, @UID = @LastUpdateBy, @CloseReasonId = NULL
END;
GO