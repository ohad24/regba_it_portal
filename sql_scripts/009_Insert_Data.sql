USE ITDB_dev
GO

INSERT INTO rts.RefSeverity (SeverityText) VALUES ('����'); --1
INSERT INTO rts.RefSeverity (SeverityText) VALUES ('������'); --2
INSERT INTO rts.RefSeverity (SeverityText) VALUES ('����'); --3

-- DELETE FROM rts.RefSeverity
-- SELECT * FROM rts.RefSeverity

INSERT INTO rts.RefNoteType (NoteTypeText) VALUES ('�����'); --1
INSERT INTO rts.RefNoteType (NoteTypeText) VALUES ('�����'); --2
INSERT INTO rts.RefNoteType (NoteTypeText) VALUES ('�����'); --3
INSERT INTO rts.RefNoteType (NoteTypeText) VALUES ('����� �����'); --4

-- DELETE FROM rts.RefTicketStatus
-- SELECT * FROM rts.RefTicketStatus
INSERT INTO rts.RefTicketStatus (TicketStatusText) VALUES ('����');
INSERT INTO rts.RefTicketStatus (TicketStatusText) VALUES ('����� �����');
INSERT INTO rts.RefTicketStatus (TicketStatusText) VALUES ('������ ������');
INSERT INTO rts.RefTicketStatus (TicketStatusText) VALUES ('����');

-- DELETE FROM rts.RefCloseReason
-- SELECT * FROM rts.RefCloseReason
INSERT INTO rts.RefCloseReason (CloseReasonText) VALUES ('������, ����');
INSERT INTO rts.RefCloseReason (CloseReasonText) VALUES ('������, �� ����');
INSERT INTO rts.RefCloseReason (CloseReasonText) VALUES ('����');

-- DELETE FROM rts.RefNoteType
-- SELECT * FROM rts.RefNoteType

INSERT INTO rts.RefCategory1 (Category1Text) VALUES ('ERP'); --1
INSERT INTO rts.RefCategory1 (Category1Text) VALUES ('����� ����'); --2
INSERT INTO rts.RefCategory1 (Category1Text) VALUES ('������'); --3

-- DELETE FROM rts.RefCategory1
-- SELECT * FROM rts.RefCategory1

--UPDATE rts.RefCategory1 SET Category1Id = 99 WHERE Category1Id = 1
--SELECT * FROM rts.RefCategory2 where ParentCategory1Id in (1, 99)

-- for erp (1)
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('������', 1); --1
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('����', 1); --2
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('������', 1); --3
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('����', 1); --4

-- for hd (2)
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('����', 2); --5
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('�����', 2); --6
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('���� �����', 2); --7
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('�������', 2); --8
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('�����', 2); --9
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('������', 2); --10

-- network (3)
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('�������', 3); --11
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('����', 3); --12
INSERT INTO rts.RefCategory2 (Category2Text, ParentCategory1Id) VALUES ('������', 3); --13

-- DELETE FROM rts.RefCategory2
-- SELECT * FROM rts.RefCategory2

-- SLA_time in hours
-- for erp (1)
	-- slowness (1)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('��� ����', 1, 24 * 3); --1
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('��� ������', 1, 24); --2
	-- malfunction (2)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('��� ����', 2, 24 * 3); --3
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('��� ������', 2, 24 * 2); --4
	-- permission (3)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('����', 3, 24 * 3); --5
	-- information (4)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('���� �� ����', 4, 24 * 3); --6
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('����� �� ����', 4, 24 * 3); --7

-- for help desk (2)
	-- computer (5)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('�� ����', 5, 24); --8
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('�� ����', 5, 24); --9
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('���� ����', 5, 24); --10
	-- printer (6)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('��� ��� ����', 6, 24); --11
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('��� ��� ������', 6, 24 * 3); --12
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('����� �� ������', 6, 24); --13
	-- extend equipment (7)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('�����', 7, 24 * 2); --14
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('����', 7, 24 * 2); --15
	-- windows (8)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('�� ���� �����', 8, 24 * 1.5); --16
	-- software (9)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('�����', 9, 24 * 7); --17
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('�����', 9, 24 * 3); --18
	-- permission (10)
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('�����', 10, 24 * 3); --19
	INSERT INTO rts.RefCategory3 (Category3Text, ParentCategory2Id, SLA_time) VALUES ('����� ���', 10, 24 * 3); --20

-- for network
	-- internet (11)

	-- office (12)

	-- permission (13)

-- DELETE FROM rts.RefCategory3
-- SELECT * FROM rts.RefCategory3




----Users Schema-----
-- DELETE FROM dbo.TeamClass
-- SELECT * FROM dbo.TeamClass

	INSERT INTO dbo.TeamClass (TeamClassName, IsOps) VALUES ('Customers', 0); --1
	INSERT INTO dbo.TeamClass (TeamClassName, IsOps) VALUES ('HD', 1); --2
	INSERT INTO dbo.TeamClass (TeamClassName, IsOps) VALUES ('Network', 1); --3
	INSERT INTO dbo.TeamClass (TeamClassName, IsOps) VALUES ('IT', 1); --4
	INSERT INTO dbo.TeamClass (TeamClassName, IsOps) VALUES ('Site Admins', 1); --5

-- DELETE FROM dbo.Teams
-- SELECT * FROM dbo.Teams
-- Customers (1)
	INSERT INTO dbo.Teams (TeamName, TeamDesc, TeamClassId) Values ('���', '���� ���', 1) --1
	INSERT INTO dbo.Teams (TeamName, TeamDesc, TeamClassId) Values ('����', '���� ����', 1) --2
	INSERT INTO dbo.Teams (TeamName, TeamDesc, TeamClassId) Values ('�����', '���� �����', 1) --3
-- HD (2)
	INSERT INTO dbo.Teams (TeamName, TeamDesc, TeamClassId) Values ('����� ����', '���� ����� ����', 2) --4
-- Network (3)
	INSERT INTO dbo.Teams (TeamName, TeamDesc, TeamClassId) Values ('���', '���� ���', 3) --5
-- IT (4)
	INSERT INTO dbo.Teams (TeamName, TeamDesc, TeamClassId) Values ('ERP', '���� ERP', 4) --6
	INSERT INTO dbo.Teams (TeamName, TeamDesc, TeamClassId) Values ('�����', '���� ����� RDL', 4) --7

-- DELETE FROM dbo.Teams
-- SELECT * FROM dbo.UserClass
	INSERT INTO dbo.UserClass (UserClassName) VALUES ('Employee');
	INSERT INTO dbo.UserClass (UserClassName) VALUES ('IT');



DECLARE @UserName NVARCHAR(MAX);
DECLARE @F_Name NVARCHAR(MAX);
DECLARE @L_Name NVARCHAR(MAX);
DECLARE @IsManager BIT;
DECLARE @UserClassId INT;
DECLARE @TeamId INT;

-- DELETE FROM dbo.Users
-- SELECT * FROM dbo.Users
	SELECT @USERNAME = 'fin1emp', @IsManager = 0, @UserClassId = 1, @TeamId = 3, @F_Name = '������', @L_Name = '���'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)

	SELECT @USERNAME = 'fin2emp', @IsManager = 0, @UserClassId = 1, @TeamId = 3, @F_Name = '�����', @L_Name = '����'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)

	SELECT @USERNAME = 'fin1mng', @IsManager = 1, @UserClassId = 1, @TeamId = 3, @F_Name = '���', @L_Name = '���'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)

	SELECT @USERNAME = 'warehouse1emp', @IsManager = 0, @UserClassId = 1, @TeamId = 2, @F_Name = '����', @L_Name = '����'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)

	SELECT @USERNAME = 'warehouse1mng', @IsManager = 1, @UserClassId = 1, @TeamId = 2, @F_Name = '�����', @L_Name = '������'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)

	SELECT @USERNAME = 'hd1emp', @IsManager = 0, @UserClassId = 2, @TeamId = 4, @F_Name = '�����', @L_Name = '���'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)

	SELECT @USERNAME = 'hd2emp', @IsManager = 0, @UserClassId = 2, @TeamId = 4, @F_Name = '����', @L_Name = '���'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)

	SELECT @USERNAME = 'hd1mng1', @IsManager = 1, @UserClassId = 2, @TeamId = 4, @F_Name = '����', @L_Name = '�����''�'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)
	--
	SELECT @USERNAME = 'erp1emp', @IsManager = 0, @UserClassId = 2, @TeamId = 6, @F_Name = '����', @L_Name = '�����'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)

	SELECT @USERNAME = 'erp2emp', @IsManager = 0, @UserClassId = 2, @TeamId = 6, @F_Name = '����', @L_Name = '����'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)

	SELECT @USERNAME = 'erp1mng', @IsManager = 1, @UserClassId = 2, @TeamId = 6, @F_Name = '�����', @L_Name = '�����'
	INSERT INTO dbo.Users (UserName, UserClassId, TeamId, F_Name, L_Name, Email, IsManager, PassKey, IsBlock, IsAuth) VALUES (@UserName, @UserClassId, @TeamId, @F_Name, @L_Name, @UserName+'@regba.co.il', @IsManager, '1234', 0, 1)


-- TEAM CAT RELATIONS
INSERT INTO rts.TeamsCat1Assignment (TeamId, Category1Id) VALUES (4, 2)
INSERT INTO rts.TeamsCat1Assignment (TeamId, Category1Id) VALUES (5, 3)
INSERT INTO rts.TeamsCat1Assignment (TeamId, Category1Id) VALUES (6, 1)
INSERT INTO rts.TeamsCat1Assignment (TeamId, Category1Id) VALUES (7, 1)