/*
SELECT t.TeamId, t.TeamName, t.TeamDesc, tc.TeamClassName FROM dbo.Teams t, dbo.TeamClass tc
WHERE t.TeamClassId = tc.TeamClassId
AND tc.IsOps = 1

SELECT * FROM dbo.Teams t
SELECT * FROM dbo.TeamClass t

select * from rts.RefCategory1
select * from rts.TeamsCat1Assignment
*/
INSERT INTO rts.TeamsCat1Assignment (TeamId, Categoty1Id) VALUES (4, 2)
INSERT INTO rts.TeamsCat1Assignment (TeamId, Categoty1Id) VALUES (5, 3)
INSERT INTO rts.TeamsCat1Assignment (TeamId, Categoty1Id) VALUES (6, 1)
INSERT INTO rts.TeamsCat1Assignment (TeamId, Categoty1Id) VALUES (7, 1)

CREATE VIEW rts.v_TeamsAndClasses as
	SELECT t.TeamId, t.TeamName, t.TeamDesc, tc.TeamClassName FROM dbo.Teams t, dbo.TeamClass tc
	WHERE t.TeamClassId = tc.TeamClassId


SELECT ot.*,
tc1.Tc1Id, tac.TeamName, tac.TeamDesc, tac.TeamClassName , ct1.Category1Text, tc1.Categoty1Id
FROM rts.TeamsCat1Assignment tc1, rts.v_TeamsAndClasses tac, rts.RefCategory1 ct1
inner JOIN rts.v_GetOpenTickets_Cust ot on ct1.Category1Id = ot.Category1Id
WHERE tc1.TeamId = tac.TeamId
AND tc1.Categoty1Id = ct1.Category1Id

--SELECT * FROM rts.Tickets
SELECT * FROM rts.v_GetOpenTickets_Cust