$(document).ready(function(){

	$("#usericons_nav").one("mouseover", function () {
		$("#usericons_nav").notify("Click me!!", "info");
	});
	
	var $ude = $('.ude');
	var old_ude_email = $('#ud_email').val();
	
	function update_user_det_by_user() {
		var email = $("#ud_email").val();
		if (email != old_ude_email) {
			//$.post($SCRIPT_ROOT + '/_update_user_presonal_detail', {email: email}, function(response) {
			$.post($SCRIPT_ROOT + '/_update_user_presonal_detail', {email: email});
			$('#ud_email').val(email);
			old_ude_email = email;
			$.notify('User details update successfully', 'success');
		} else {
			$.notify('No Change', 'info');
		};
		$("#userpresdet").html("Edit");
		$ude.attr('readonly', true).attr('contenteditable','false').removeAttr('style');
	};
	
	$('#submit_pers_det').on("click", function () {
		// console.log($("#ud_email").attr('readonly'));
		if ($("#ud_email").attr('readonly') == undefined) {
			update_user_det_by_user()
		}	
		});
	
	$('#userpresdet').click(function() {
		if ($ude.attr('readonly')) {
			$ude.removeAttr('readonly')
			.attr('contenteditable','true')
			.css({"color":"green","border":"2px solid green"});
			$("#userpresdet").html("Cancel")
			
		} else {
			$ude.attr('readonly', true).attr('contenteditable','false').removeAttr('style');
			$('#ud_email').val(old_ude_email);
			$("#userpresdet").html("Edit")
		}
	});
});